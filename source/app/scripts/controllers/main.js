'use strict';

/**
 * @ngdoc function
 * @name sourceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sourceApp
 */
angular.module('sourceApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.commits = [];
    $scope.getDifference = function(date) {
      return moment(date).fromNow();
    };

    $scope.endInNumber = function(char) {
      return !isNaN(char.slice(-1));
    };

    $scope.getMsgTitle = function(msg) {
      return msg.split("\n")[0];
    };

    $scope.refresh = function() {
      $scope.commits = [];
      $http({
        method: 'GET',
        url: 'https://api.github.com/repos/angular/angular.js/commits',
        dataType: 'json',
        headers: {
            'Content-Type': 'application/json'
        }
      })
      .success(function(commits) {
        $scope.commits = commits;
      })
    }
    $scope.refresh();
  });
